# README OF ANGULARJS TEST SLIDER FOR STELLA&DOT #

This application was developed for a technical test for Stella&Dot

## Technologies ##

This Application is made with:
* AngularJS
* HTML
* CSS
* Bootstrap
* Slick Slider
* Custom bootstrap theme called Journal with some customizations

## This Application allows you to: ##

*  View some random photographs of different group of jewels

## Technical Documentation ##

### Controllers ###

* MainCtrl(main.js): Controller for show section

### Views ###

* main.html: Show different images in slick slider format

### Services ###

* JewelsService: Service that contains an API example response and retrieve random list of photos

### Methods of Jewels Service ###

* Get random images.

### Directives ###

   * slickSlider: Allows to control slick slider and prevents break up

### Repository owner ###

Sonia Benítez Martínez