/**
 * @ngdoc function
 * @name stelladot.factory:jewels
 * @description Work with jewel list
 *
 */

angular.module("stelladotApp")
    .factory("jewelsService", function ($http, $location, $route) {
        //Example of API response
        var apiInfo = [
            {"title": "First Block",
                "images": [
                    "http://shop.stelladot.com/style/media/catalog/product/n/4/n491em_eye_candy_necklace_emerald_main_1.jpg",
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/n/4/n491em_eye_candy_necklace_emerald_bust_1.jpg",
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/n/4/n491em_eye-candy-necklace_emerald_model_3.jpg"
                ]
            },
            {"title": "Second Block",
                "images": [
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/n/4/n491hp_eye_candy_necklace_hot_pink_main_1.jpg",
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/n/4/n491hp_eye_candy_necklace_hot_pink_bust_1.jpg"
                ]
            },
            {"title": "Third Block",
                "images": [
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/n/4/n489cr_vintage_crystal_necklace_main.jpg",
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/n/4/n489cr_vintage_crystal_necklace_bust.jpg",
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/b/3/b307cr_casablanca-bracelet_model.jpg"
                ]
            },
            {"title": "Fourth Block",
                "images": [
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/n/4/n490cr_casablanca_pendant_necklace_main.jpg",
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/n/4/n490cr_casablanca-pendant-necklace_model.jpg"
                ]
            },
            {"title": "Fifth Block",
                "images": [
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/n/4/n416_phoenix_main.jpg",
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/d/2/d2_11_mardel_1813_smp_e_660_1000.jpg"
                ]
            },
            {"title": "Sixth Block",
                "images": [
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/n/3/n391s_somerveil_silver_main_1.jpg"
                ]
            },
            {"title": "Seventh Block",
                "images": [
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/b/3/b306cr_vintage_crystal_bracelet_main.jpg",
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/b/3/b306cr_vintage_crystal_bracelet_model_5.jpg",
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/b/3/b306cr_vintage_crystal_bracelet_alt.jpg"
                ]
            },
            {"title": "Eighth Block",
                "images": [
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/b/2/b246s_christina_1_1.jpg",
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/c/h/christinagold.jpg",
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/b/2/b246g_christina_1_2.jpg",
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/b/2/b246s_christina_2_1.jpg"
                ]
            },
            {"title": "Ninth Block",
                "images": [
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/b/2/b282s_amelie_sparkle_main_rgb_a_1.jpg",
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/d/2/d2_02abc_mardel_1270_smp_d_660_1000_1.jpg",
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/b/2/b282s_amelie_sparkle_bracelet_top_1.jpg"
                ]
            },
            {"title": "Tenth Block",
                "images": [
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/e/2/e221bl_seychelles_1_1.jpg",
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/e/2/e221p_seychelles_model.jpg"
                ]
            },
            {"title": "Eleventh Block",
                "images": [
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/r/1/r160s5_deco-cocktail-ring_main_6.jpg",
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/r/1/r160s5_deco-cocktail-ring_alt_6.jpg"
                ]
            },
            {"title": "Twelfth Block",
                "images": [
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/r/8/r805s_1_6.jpg",
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/r/8/r805s_deco_rings_3.jpg",
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/r/8/r805s_2_7.jpg",
                    "http://shop.stelladot.com/style/media/catalog/product/cache/0/image/450x682/9df78eab33525d08d6e5fb8d27136e95/r/8/r805s_3_6.jpg"
                ]
            }
        ]
        var tmpJewel = {};
        var jewels = [];

        return{
            //Retrieve all jewels
            getRandomImages: function () {
                jewels = [];
                tmpJewel = apiInfo;
                //Loop all apiInfo
                angular.forEach(apiInfo, function (value, key) {
                    //If any element of api has images
                    if (value.hasOwnProperty("images")) {
                        //Check how many images are in single element
                        var imagesLength = value.images.length;
                        //Retrieve random image and add to jewels array
                        var photoNumber = parseInt(Math.random() * (imagesLength - 0) + 0);
                        jewels.push(value.images[photoNumber]);
                    }
                });
                return jewels;
            }
        }
    });