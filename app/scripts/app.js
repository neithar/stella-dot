'use strict';

/**
 * @ngdoc overview
 * @name stelladotApp
 * @description
 * # stelladotApp
 *
 * Main module of the application.
 */
angular
  .module('stelladotApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'slick'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
