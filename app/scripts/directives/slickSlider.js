'use strict';

/**
 * @ngdoc function
 * @name stelladotApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the stelladotApp
 */
angular.module('stelladotApp').
    directive('slickSlider', function () {
        return {
            restrict: 'A',
            scope: {'data': '='},
            link: function (scope, element, attrs) {
                var isInitialized = false;
                scope.$watch('data', function(newVal, oldVal) {
                    if (newVal.length > 0 && !isInitialized) {
                        $(element).slick(scope.$eval(attrs.slickSlider));
                        isInitialized = true;
                    }
                });
            }
        }
    });
