'use strict';

/**
 * @ngdoc function
 * @name stelladotApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the stelladotApp
 */
angular.module('stelladotApp')
  .controller('MainCtrl', function ($scope, $http, $location, jewelsService, $timeout) {
        $scope.index = 0;
        $scope.images = jewelsService.getRandomImages();

        $timeout(function(){
            //Call service to retrieve random images of any block of elements
           $scope.images = jewelsService.getRandomImages();
           $scope.dataLoaded = true;
        }, 2000);

        $scope.messItUp = function(){
            $scope.dataLoaded = false;
            //Call service to retrieve random images of any block of elements
            $scope.images = jewelsService.getRandomImages();
            // the slick directive doesn't expose the reinit method to the public api
            // I trick it out with ng-include and ng-if attribute.
            // I put it in the timeout function in order to get the prevoiusly elements completly removed.
            // otherwise the old elements stay in the directive and the carousel breaks
            $timeout(function(){
                $scope.dataLoaded = true;
            });

        }
  });
